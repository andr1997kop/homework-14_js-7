/* 
1. Метод forEach дозволяє перебрати всі елементи масиву
та виконита з кожним з них певну дію.
2. Можна просто присвоїти порожній масив змінній, у якої
значеням був непорожній масив.
3. Можна використати метод isArray. Він повертає булеве значення.
 */

let array = [0, 2, `string`, true, 8, undefined, BigInt];

function filterBy(getArray, type) {
    let newArr = getArray.filter(elem => typeof(elem) !== type);
    return newArr;
};
let result = filterBy(array, `string`);
console.log(result);
